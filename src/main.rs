extern crate rand;

use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main(){
    println!("Guess the secret number");
    println!("Input a number between 1 and 101: ");
    
    let mut guess = String::new();
    io::stdin().read_line(&mut guess).expect("Failed to read line");

    let guess: u32 = guess.trim().parse().expect("Please input a number");

    let secret_num = rand::thread_rng().gen_range(1, 101);

    println!("The secret number is: {}", secret_num);

    println!("You guessed: {}", guess);

    match guess.cmp(&secret_num){
        Ordering::Less => println!("Sorry, your guess was too low"),
        Ordering::Greater => println!("Sorry, your guess was too high"),
        Ordering::Equal => println!("Alright!!!! \nYour guess was correct"),
    }

}
