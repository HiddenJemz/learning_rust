/* Practice makes impl 
 *
 */ 

use std::io::stdin;
use pause_console::*;


fn main(){
 
    let mut userIn = String::new();
    
    println!("Hello!\n\nPlease enter a number between 1 - 100: ");
    stdin().read_line(&mut userIn).ok().expect("Invalid entry!\nYour system will shutdown..");

    //covert string to i32
    let userIn: i32 = userIn.trim().parse().unwrap();


    if userIn > 100 || userIn <= 0{
        
        pause_console!("Press any key to erase the SSD!!\nGood Bye!");
        println!("The value has broken the rules!!\nYour system will shutdown..");

    }//close if 


    println!("Thank you!\nYou have entered: {}", userIn);

    match userIn{
        
        1|10 => println!("One for the money!"),
        2|20 => println!("Two for the show!"),
        3|30 => println!("Three to get ready..."),
        4|40 => println!("On FOUR let GOOOO!!"),
        _ => println!("You\'re just wack!!! SMH..")

    }//close match     

}//close main
 //Work on inserting a timer to pause the system.
